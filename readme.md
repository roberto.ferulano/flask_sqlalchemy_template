# Backend in Python con Flask, SqlAlchemy e API che usano Swagger

Questo template è un server completo che contiene:
- Flask - un server flask
- SqlAlchemy - le librerie di Sql Alchemy per la connessione ad un DB esterno (Mysql, Postgresql)
- API - un alberatura di API commentate con Swagger


## Installazione
Il template è stato testato su Python 3.7.
Come per ogni progetto Python, si consiglia di costruire un [ambiente virtuale](https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/) e lanciare il comando di installazione delle librerie

~~~~
pip install -r requirements.txt
~~~~
Potrebbero esserci problemi con il connettore mysql. Si consiglia di installarlo [manualmente](https://pynative.com/install-mysql-connector-python/)

# Struttura del template
Il template è costruito secondo una logica MVC. 
- Model: è la cartella model
- Controller: è la cartella controller 
- View: è la cartella apis

I dati di connessione sono nel file config.ini
Il codice parte da app.py

Si riporta un file database_esempio.sql (realizzato in Mysql 8.0).
si riporta anche un file per la dockerizzazione (Dockerfile)

## Model
Questo tool parte dall'idea che il database è già pronto e lui deve solo crearsi gli oggetti.
Nel file database.py vengono inizializzati gli oggetti del DB. Notare che viene creato un oggetto app_db. Questo oggetto è sostanzialmente app (ossia l'istanza del server Flask), ma in questa maniera è stato reso disponibile in tutto il codice
Nel file models.py devono essere elencate tutte le tabelle del database. Il software poi recupera i dati 


## Controller
Questa parte è composta di due sottocartelle:
- objects
- functions

### objects
Questa parte dovrebbe contenere gli oggetti che devono essere istanziati (logica ad oggetti).
C'è un esempio legato alla tabella Esempio, per entrare nel mood

### functions
Questa parte contiene alcune funzioni utili:
- from_db_data_tolist per convertire i dati da DB a lista
- to_json per trasformare ogni genere di dati in json

## View o API
Si trova nella cartella apis.
Nel file __init__.py vanno aggiunti tutti i namespace che si vogliono avere.
Ossia, desidero avere un'alberatura delle mie api piuttosto ricca del tipo:
- www.sitoesempio.com\
- www.sitoesempio.com\oggetti
- www.sitoesempio.com\persone

Se ognuno di questi endpoint ha numerosi endpoints sotto di sè, posso creare tre files:
- base.py per www.sitoesempio.com\
- oggetti.py per www.sitoesempio.com\oggetti
- persone.py per www.sitoesempio.com\persone

E poi aggiungere in __init__.py i seguenti comandi

~~~~
api.add_namespace(base)
api.add_namespace(oggetti)
api.add_namespace(persone)
~~~~

Infine in ogni file devo indicare quale è il path iniziale. Ad esempio in base.py aggiungo la seguente istruzione

~~~~
api = Namespace('', description='API base')
~~~~

Mentre nel file oggetti.py aggiungo

~~~~
api = Namespace('oggetti', description='API relativi agli oggetti')
~~~~

