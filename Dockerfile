FROM ubuntu:latest

# Install Python and needs
RUN apt -yqq update && apt -yqq install software-properties-common && add-apt-repository ppa:deadsnakes/ppa && apt -y update && apt -yqq install python3.7 && apt -yqq install python3-pip default-libmysqlclient-dev
RUN apt install -yqq unzip curl wget git

# Set workdir and copy all flies
WORKDIR /appbackend

# Copy only requirements so cache works
COPY ./requirements.txt /appbackend

# Install Requirements
RUN pip3 install -U setuptools
RUN pip3 install -r /appbackend/requirements.txt
# RUN pip3 install -e git+git://github.com/mrhwick/schedule@master#egg=schedule

# Copy the rest of the project
COPY . /appbackend

# Set display port to avoid crash
ENV DISPLAY=:99
ENV DBUS_SESSION_BUS_ADDRESS=/dev/null

CMD python3 /appbackend/app.py