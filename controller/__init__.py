from model.models import *
from controller.function.utils import from_db_data_tolist, remove_field_json

import logging
import configparser


session = db.session

logger = logging.getLogger(__name__)

parser = []


def init_parser(filename):
    global parser
    parser = configparser.ConfigParser()
    parser.read(filename)


def get_configure(section, param):
    return parser.get(section, param)

