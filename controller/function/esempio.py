from model.models import *
import logging
from model.database import app_db

session = db.session

logger = logging.getLogger(__name__)


def get_example(id_prova):
    """
    """
    logger.info("Start get example")
    with app_db.app_context():
        x = []
        try:
            data = session.query(t_esempio_db.value, t_esempio_db.date)\
                .filter(t_esempio_db.id_prova == id_prova).all()
            if len(data) > 0:
                y = {"a": id,
                    "b": []}
                for i in data:
                    y["data"].append({"a": i[0], "b": i[1]})
                x.append(y)
        except Exception:
            logger.exception("error in get_example")
        return x

