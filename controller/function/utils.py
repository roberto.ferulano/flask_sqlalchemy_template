import os
import shutil
from datetime import date

import decimal

import logging
import pandas as pd
import json


import os
from datetime import date

logger = logging.getLogger(__name__)


def from_db_data_tolist(data):
    """
    it converts any type of db.
    e praticamente impossibile capire che tipo di dati sono. La cosa migliore e andare a tentativi
    :param data:
    :type data:
    :return:
    :rtype:
    """
    result = []
    if data:
        try:
            for u in data:
                row = u.__dict__
                row = remove_field_json(row, '_sa_instance_state')
                result.append(row)
            return result
        except:
            result = []
            pass

        try:
            for u in data:
                result.append(u._asdict())
            return result
        except:
            result = []
            pass

        try:
            row = data.__dict__
            row = remove_field_json(row, '_sa_instance_state')
            result.append(row)
            return result
        except:
            logger.error('errore nella funzione from_db_data_tolist ')
            result = None
    return result


def remove_field_json(data, field):  # converte la lista di oggetti in una lista di dizionario per il json
    try:
        del data[field]
    except:
        pass
    return data


def __empty_folder__(dir_name):
    """
    Funzione che svuota la cartella temporanea dove si salvano le immagini
    dopo la conversione in maniera tale da
    ottimizzare memoria.
    :param dir_name:
    :type dir_name:
    :return:
    :rtype:
    """
    if os.path.isdir(dir_name):
        shutil.rmtree(dir_name)
    os.mkdir(dir_name)


def to_json(object2):
    return json.dumps(object2, default=lambda o: __serialize__(o),
                      sort_keys=True, indent=4)


def __serialize__(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, date):
        return obj.isoformat()
    if isinstance(obj, decimal.Decimal):
        return str(obj)
    elif isinstance(obj, pd.DataFrame):
        if not obj.empty:
            return pd.DataFrame.to_dict(obj, orient='records')
        else:
            return {}

    # if isinstance(obj, time):
    #     serial = obj.isoformat()
    #     return serial
    return obj.__dict__

