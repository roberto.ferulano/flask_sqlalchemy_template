from model.models import *
from controller.function.utils import from_db_data_tolist
import pandas as pd
from model.database import app_db
from model.models import t_esempio_db
import logging
from datetime import date

session = db.session
logger = logging.getLogger(__name__)


class EsempioClass:
    def __init__(self):
        self.id = 0
        self.value = 0.0
        self.date = date.today()
        self.id_prova = 1

    def populate(self, id, value, date, id_prova):
        self.id = id
        self.value = value
        self.date = date
        self.id_prova = id_prova

    def populate_from_db(self, id_db):
        self.id = t_esempio_db.id
        self.value = t_esempio_db.value
        self.date = t_esempio_db.date
        self.id_prova = t_esempio_db.id_prova

    def populate_from_dict(self, prova_dict):
        self.id = prova_dict['id']
        self.value = prova_dict['value']
        self.date = prova_dict['date']
        self.id_prova = prova_dict['id_prova']

    def get_by_id(self):
        with app_db.app_context():
            query = session.query(t_esempio_db).filter(t_esempio_db.id == self.id).all()
            return from_db_data_tolist(query)

    def add(self):
        try:
            with app_db.app_context():
                esempio_db = t_esempio_db(
                    value=self.value,
                    date=self.date,
                    id_prova=self.id_prova)

                if self.id != 0:
                    esempio_db.id = self.id

                try:
                    session.merge(esempio_db)
                    session.commit()
                    return 200
                except:
                    session.rollback()
                    logger.exception("Error adding something")
                    raise Exception
        except:
            logger.exception("Error creation something")