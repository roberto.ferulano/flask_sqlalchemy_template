from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.automap import automap_base

db = SQLAlchemy()
app_db = None


def initialize_db(app):
    global app_db
    app_db = app
    app_db.app_context().push()
    db.init_app(app_db)
    db.reflect(app=app_db)
    with app_db.app_context():
        db.Model = automap_base(db.Model)
        db.Model.prepare(db.engine, reflect=True)





