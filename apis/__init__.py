#!/usr/bin/python3
# coding=utf-8
from flask_restplus import Api

from .base import api as base
from .log_api import api as log


api = Api(
    title='esempio',
    version='1.0',
    description='To manage a generic web app',
    # All API metadatas
)


api.add_namespace(base)
api.add_namespace(log)
