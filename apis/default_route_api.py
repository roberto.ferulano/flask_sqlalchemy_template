from flask import make_response, render_template
from flask_restplus import Namespace, Resource
import logging

logger = logging.getLogger(__name__)

api = Namespace('Default Route', description="Refresh page")


# funzione per refresh pagina
@api.route('/<path:path>', methods=['GET'], doc=False)
class Catch(Resource):
    def get(self, path):
        logger.info("Start api redirect for all pages")
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('index.html'), 200, headers)
