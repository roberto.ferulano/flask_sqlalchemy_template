from flask_restplus import Namespace, Resource, inputs
from flask import render_template, make_response, Response
import logging
from controller.objects.esempio_class import EsempioClass
from controller.function.utils import to_json
import json

logger = logging.getLogger(__name__)

api = Namespace('', description='API base')

id_parser = api.parser()
id_parser.add_argument('id', type=int, required=True)


esempio_parser = api.parser()
esempio_parser.add_argument("value", type=float, required=True)
esempio_parser.add_argument('date', type=inputs.date, required=True)
esempio_parser.add_argument("id_prova", type=int, required=True)


@api.route('/')
class Base(Resource):

    def get(self):
        logger.info("start render index.html")
        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('index.html'), 200, headers)


@api.route('/get_esempio_by_id', methods=['GET'])
@api.expect(id_parser)
class GetEsempioByID(Resource):
    @api.doc(responses={200: 'OK', 500: 'server error'})
    def get(self):
        args = id_parser.parse_args()
        id = args['id']

        esempio = EsempioClass()
        esempio.id = id
        esempio.get_by_id()
        try:
            return Response(response=to_json(esempio))
        except:
            return Response(response=json.dumps("""{ERROR}""", default=str), status=500,
                            mimetype='application/json')


@api.route('/insert_esempio', methods=['PUT'])
@api.expect(esempio_parser)
class InsertEsempio(Resource):
    @api.doc(responses={200: 'OK', 500: 'server error'})
    def put(self):
        args = esempio_parser.parse_args()
        value = args['value']
        date = args['date']
        id_prova = args['id_prova']

        esempio = EsempioClass()
        esempio.populate(0, value, date, id_prova)
        esempio.add()
        try:
            return Response(response="oggetto inserito", status=200)
        except:
            return Response(response=json.dumps("""{ERROR}""", default=str), status=500,
                            mimetype='application/json')


@api.route('/update_esempio', methods=['PUT'])
@api.expect(esempio_parser, id_parser)
class UpdateEsempio(Resource):
    @api.doc(responses={200: 'OK', 500: 'server error'})
    def put(self):
        args = esempio_parser.parse_args()
        value = args['value']
        date = args['date']
        id_prova = args['id_prova']

        args = id_parser.parse_args()
        id = args['id']

        esempio = EsempioClass()
        esempio.populate(id, value, date, id_prova)
        esempio.add()
        try:
            return Response(response="oggetto inserito", status=200)
        except:
            return Response(response=json.dumps("""{ERROR}""", default=str), status=500,
                            mimetype='application/json')

