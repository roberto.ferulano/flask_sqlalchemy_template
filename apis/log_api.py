import json

from flask_restplus import Namespace, Resource
from flask import Response, send_from_directory, send_file

import os
import logging
from flask import request
from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename


logger = logging.getLogger(__name__)

api = Namespace('log', description='log api')
id_parser = api.parser()
id_parser.add_argument('id', type=int, required=True)

str_parser = api.parser()
str_parser.add_argument("name", type=str, required=True)

file_parser = api.parser()
file_parser.add_argument("file", type=FileStorage, location='files', required=True)


@api.route('/get_logs', methods=['GET'])
class GetLogs(Resource):
    @api.doc(responses={200: 'OK', 500: 'server error'})
    def get(self):
        """

         """
        try:
            try:
                arr = os.listdir('./log/logbase/')
                arr = arr[::-1]
                return Response(response=json.dumps(arr, default=str), status=200,
                                mimetype='application/json')
            except:
                return Response(response=json.dumps("[]"), status=200,
                                mimetype='application/json')
        except Exception as e:
            logger.exception(e)
            return Response(response=json.dumps("{""ERROR""}", default=str), status=500,
                            mimetype='application/json')


@api.route('/get_log_by_name', methods=['GET'])
@api.expect(str_parser)
class GetLogByName(Resource):
    @api.doc(responses={200: 'OK', 500: 'server error'})
    def get(self):
        """
         Get log by name
         :return: download file
         :rtype: file
         """
        try:
            args = str_parser.parse_args()
            data = args['name']

            try:
                return send_from_directory("log", filename=str(data), as_attachment=True)
            except:
                return Response(response=json.dumps("""{ERROR}""", default=str), status=500,
                                mimetype='application/json')
        except Exception as e:
            logger.exception(e)
            return Response(response=json.dumps("{""ERROR""}", default=str), status=500,
                            mimetype='application/json')

