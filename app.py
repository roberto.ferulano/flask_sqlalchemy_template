import os
import logging
from flask_cors import CORS
from config import PATH_LOG, PATH_DIR
from flask import Flask, request, redirect, url_for
from model.database import  initialize_db


#creazione file logging
# from model.database import initialize_db
from datetime import date

logger_app = logging.getLogger(__name__)
logger_app.setLevel(logging.INFO)
logging.captureWarnings(False)
logging.basicConfig(format='%(asctime)s ~ %(process)d ~ %(name)s ~ %(levelname)s ~ %(message)s ',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    filename=PATH_DIR + PATH_LOG + 'log_app_' + date.today().strftime("%Y-%m-%d") + ".log",
                    filemode='a')

app = Flask(__name__)

app.config.from_object('config')
initialize_db(app)


# start
if __name__ == '__main__':
    CORS(app)
    from apis import api
    api.init_app(app)
    port = int(os.getenv('PORT', 5000))
    # from scheduler import myThreadSchedule
    # t = myThreadSchedule()
    # t.start()
    app.run(debug=False, port=port, host='0.0.0.0')
