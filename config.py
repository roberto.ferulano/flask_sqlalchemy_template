import configparser
import os

configApp = configparser.ConfigParser()
configApp.read(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'config.ini'))

# DATABASE
DB_USER = os.environ.get("DB_USER") or configApp['DB']['DB_USER']
DB_PASSWORD = os.environ.get("DB_PASSWORD") or configApp['DB']['DB_PASSWORD']
DB_HOST = os.environ.get("DB_HOST") or configApp['DB']['DB_HOST']
DB_PORT = os.environ.get("DB_PORT") or configApp['DB']['DB_PORT']
DB_NAME = os.environ.get("DB_NAME") or configApp['DB']['DB_NAME']


SQLALCHEMY_DATABASE_URI = 'mysql+mysqldb://' + \
                          DB_USER + ':' + DB_PASSWORD + '@' + \
                          DB_HOST + ':' + DB_PORT + '/' + \
                          DB_NAME + '?charset=latin1&binary_prefix=true'
SQLALCHEMY_TRACK_MODIFICATIONS = False  # per rimuovere l'avviso degli eventi flask-sqlalchemy

# PATH
PATH_DIR = os.environ.get("PATH_DIR") or configApp['PATH']['PATH_DIR']
PATH_LOG = os.environ.get("PATH_LOG") or configApp['PATH']['PATH_LOG']



